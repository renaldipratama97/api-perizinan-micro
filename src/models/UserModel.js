import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const User = db.define(
  "user",
  {
    kode_user: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    nik: {
      type: DataTypes.STRING(20),
    },
    username: {
      type: DataTypes.STRING(20),
    },
    password: {
      type: DataTypes.STRING(64),
    },
    user_level: {
      type: DataTypes.STRING(20),
    },
    picture: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default User;
