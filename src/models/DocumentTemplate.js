import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const DocumentTemplate = db.define(
  "document_template",
  {
    kode_document_template: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    jenis_izin_usaha: {
      type: DataTypes.STRING(20),
    },
    url_file: {
      type: DataTypes.STRING(200),
    },
    status: {
      type: DataTypes.INTEGER(3),
    },
  },
  {
    freezeTableName: true,
  }
);
export default DocumentTemplate;
