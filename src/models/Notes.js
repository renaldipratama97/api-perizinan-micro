import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Notes = db.define(
  "notes",
  {
    kode_note: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    kode_permohonan_izin: {
      type: DataTypes.STRING(64),
    },
    deskripsi_note: {
      type: DataTypes.TEXT,
    },
    tgl_note: {
      type: DataTypes.DATE,
    },
  },
  {
    freezeTableName: true,
  }
);
export default Notes;
