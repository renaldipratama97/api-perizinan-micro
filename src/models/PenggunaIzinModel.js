import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const PenggunaIzin = db.define(
  "pengguna_izin",
  {
    kode_pengguna_izin: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    nik: {
      type: DataTypes.STRING(20),
    },
    nama_lengkap: {
      type: DataTypes.STRING(35),
    },
    tempat_lahir: {
      type: DataTypes.STRING(25),
    },
    tgl_lahir: {
      type: DataTypes.DATEONLY,
    },
    jenkel: {
      type: DataTypes.STRING(15),
    },
    agama: {
      type: DataTypes.STRING(15),
    },
    pekerjaan: {
      type: DataTypes.STRING(25),
    },
    no_handphone: {
      type: DataTypes.STRING(15),
    },
    alamat: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default PenggunaIzin;
