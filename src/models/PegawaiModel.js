import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Pegawai = db.define(
  "pegawai",
  {
    kode_pegawai: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    nik: {
      type: DataTypes.STRING(20),
    },
    nama_lengkap: {
      type: DataTypes.STRING(35),
    },
    jenkel: {
      type: DataTypes.STRING(15),
    },
    agama: {
      type: DataTypes.STRING(15),
    },
    jabatan: {
      type: DataTypes.STRING(20),
    },
    no_handphone: {
      type: DataTypes.STRING(15),
    },
    status_pegawai: {
      type: DataTypes.STRING(15),
    },
    alamat: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default Pegawai;
