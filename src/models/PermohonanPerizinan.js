import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const PermohonanPerizinan = db.define(
  "permohonan_perizinan",
  {
    kode_permohonan_perizinan: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    kode_pengguna_izin: {
      type: DataTypes.STRING(64),
    },
    jenis_izin_usaha: {
      type: DataTypes.STRING(20),
    },
    nama_usaha: {
      type: DataTypes.STRING(30),
    },
    bidang_usaha: {
      type: DataTypes.STRING(30),
    },
    alamat_usaha: {
      type: DataTypes.STRING(200),
    },
    tgl_permohonan_izin: {
      type: DataTypes.DATE,
    },
    status_permohonan_izin: {
      type: DataTypes.INTEGER(3),
    },
    file_permohonan_izin: {
      type: DataTypes.STRING(200),
    },
    reviewer: {
      type: DataTypes.STRING(64),
    },
  },
  {
    freezeTableName: true,
  }
);
export default PermohonanPerizinan;
