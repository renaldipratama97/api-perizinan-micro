import DocumentTemplate from "../models/DocumentTemplate.js";
import { v4 as uuidv4 } from "uuid";

export const getDocumentTemplate = async (req, res) => {
  try {
    const document = await DocumentTemplate.findAll({
      where: {
        status: 1,
      },
    });

    res.json({ statusCode: 200, message: "success", data: document });
  } catch (error) {
    console.log(error);
  }
};

export const createDocumentTemplate = async (req, res) => {
  try {
    if (!req.file) {
      return res.json({
        statusCode: 422,
        message: "Document harus di Upload",
      });
    }

    const jenis_izin_usaha = req.body.jenis_izin_usaha;
    const url_file = req.file.path;

    await DocumentTemplate.create({
      kode_document_template: uuidv4(),
      jenis_izin_usaha,
      url_file,
      status: 0,
    });
    res.json({ statusCode: 201, message: "success" });
  } catch (error) {
    console.log(error);
  }
};
