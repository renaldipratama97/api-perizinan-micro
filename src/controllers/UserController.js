import User from "../models/UserModel.js";
import PenggunaIzin from "../models/PenggunaIzinModel.js";
import { v4 as uuidv4 } from "uuid";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export const getUserByNIK = async (req, res) => {
  try {
    const nik = req.params.nik;

    const user = await User.findOne({
      where: { nik },
    });

    const pengguna_izin = await PenggunaIzin.findOne({
      where: { nik },
    });

    if (!user && !pengguna_izin) {
      return res.json({ statusCode: 404, message: "data not found" });
    }

    res.json({
      statusCode: 200,
      message: "success",
      data: {
        user: user,
        pengguna_izin: pengguna_izin,
      },
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateUser = async (req, res) => {
  try {
    if (!req.file) {
      return res.json({
        statusCode: 422,
        message: "Picture harus di Upload",
      });
    }
    const {
      nik,
      nama_lengkap,
      tempat_lahir,
      tgl_lahir,
      jenkel,
      agama,
      pekerjaan,
      no_handphone,
      alamat,
      username,
    } = req.body;

    await PenggunaIzin.update(
      {
        nama_lengkap,
        tempat_lahir,
        tgl_lahir,
        jenkel,
        agama,
        pekerjaan,
        no_handphone,
        alamat,
      },
      {
        where: {
          nik,
        },
      }
    );

    await User.update(
      {
        username,
        picture: req.file.path,
      },
      {
        where: {
          nik,
        },
      }
    );

    res.json({ statusCode: 201, message: "update user success" });
  } catch (error) {
    res.json({ statusCode: 500, message: error });
  }
};

export const Register = async (req, res) => {
  try {
    const { nik, nama_lengkap, username, password } = req.body;

    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);

    const findUser = await User.findOne({
      where: {
        nik,
      },
    });

    const findUserByUsername = await User.findOne({
      where: {
        username,
      },
    });

    if (findUser)
      return res.json({
        statusCode: 304,
        message: "nik sudah terdaftar di database",
      });

    if (findUserByUsername)
      return res.json({
        statusCode: 304,
        message: "Username sudah ada yang gunakan",
      });

    await PenggunaIzin.create({
      kode_pengguna_izin: uuidv4(),
      nik,
      nama_lengkap,
    });

    await User.create({
      kode_user: uuidv4(),
      nik,
      username,
      password: hashPassword,
      user_level: "Pengguna Izin",
    });
    res.json({ statusCode: 201, message: "success" });
  } catch (error) {
    console.log(error);
  }
};

export const Login = async (req, res) => {
  const { username, password } = req.body;

  // Check for user username
  const user = await User.findOne({ where: { username: username } });

  if (!user) {
    return res
      .status(404)
      .json({ statusCode: 404, message: "username tidak ada di database" });
  }
  if (user) {
    const match = await bcrypt.compare(password, user.password);

    const pengguna = await PenggunaIzin.findOne({
      where: { nik: user.nik },
    });

    if (!match) {
      return res
        .status(400)
        .json({ statusCode: 400, message: "password salah!!!" });
    }

    if (match) {
      res.json({
        pengguna_izin: pengguna,
        kode_user: user.kode_user,
        nik: user.nik,
        username: user.username,
        user_level: user.user_level,
        token: generateToken(
          user.kode_user,
          user.nik,
          user.username,
          user.user_level
        ),
      });
    } else {
      res.status(400);
      throw new Error("Invalid credentials");
    }
  }
};

const generateToken = (kode_user, nik, username, user_level) => {
  return jwt.sign(
    { kode_user, nik, username, user_level },
    process.env.ACCESS_TOKEN,
    {
      expiresIn: "1d",
    }
  );
};
