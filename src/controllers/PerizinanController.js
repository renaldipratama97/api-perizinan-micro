import PermohonanIzin from "../models/PermohonanPerizinan.js";
import { v4 as uuidv4 } from "uuid";

export const getPermohonanPerizinan = async (req, res) => {
  try {
    const kode = req.params.kode_pengguna_izin;
    const permohonanIzin = await PermohonanIzin.findAll({
      where: {
        kode_pengguna_izin: kode,
      },
    });

    res.json({ statusCode: 200, message: "success", data: permohonanIzin });
  } catch (error) {
    console.log(error);
  }
};

export const createPermohonanPerizinan = async (req, res) => {
  try {
    if (!req.file) {
      return res.json({
        statusCode: 422,
        message: "Document harus di Upload",
      });
    }

    const kode_pengguna_izin = req.body.kode_pengguna_izin;
    const jenis_izin_usaha = req.body.jenis_izin_usaha;
    const nama_usaha = req.body.nama_usaha;
    const bidang_usaha = req.body.bidang_usaha;
    const alamat_usaha = req.body.alamat_usaha;
    const file_permohonan_izin = req.file.path;

    await PermohonanIzin.create({
      kode_permohonan_perizinan: uuidv4(),
      kode_pengguna_izin,
      jenis_izin_usaha,
      nama_usaha,
      bidang_usaha,
      alamat_usaha,
      tgl_permohonan_izin: new Date(),
      status_permohonan_izin: 1,
      file_permohonan_izin,
    });
    res.json({ statusCode: 201, message: "success create permohonan izin" });
  } catch (error) {
    console.log(error);
  }
};

export const updatePermohonanPerizinan = async (req, res) => {
  try {
    if (!req.file) {
      return res.json({
        statusCode: 422,
        message: "Document harus di Upload",
      });
    }

    const kode_permohonan_perizinan = req.body.kode_permohonan_perizinan;
    const jenis_izin_usaha = req.body.jenis_izin_usaha;
    const nama_usaha = req.body.nama_usaha;
    const bidang_usaha = req.body.bidang_usaha;
    const alamat_usaha = req.body.alamat_usaha;
    const file_permohonan_izin = req.file.path;

    await PermohonanIzin.update(
      {
        jenis_izin_usaha,
        nama_usaha,
        bidang_usaha,
        alamat_usaha,
        tgl_permohonan_izin: new Date(),
        status_permohonan_izin: 1,
        file_permohonan_izin,
      },
      {
        where: {
          kode_permohonan_perizinan,
        },
      }
    );
    res.json({ statusCode: 200, message: "success update permohonan izin" });
  } catch (error) {
    console.log(error);
  }
};
