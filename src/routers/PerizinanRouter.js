import express from "express";
import {
  getPermohonanPerizinan,
  createPermohonanPerizinan,
  updatePermohonanPerizinan,
} from "../controllers/PerizinanController.js";

const router = express.Router();

router.get("/perizinan/get/:kode_pengguna_izin", getPermohonanPerizinan);
router.post("/perizinan/create", createPermohonanPerizinan);
router.post("/perizinan/update", updatePermohonanPerizinan);

export default router;
