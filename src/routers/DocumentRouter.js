import express from "express";
import {
  getDocumentTemplate,
  createDocumentTemplate,
} from "../controllers/DocumentController.js";

const router = express.Router();

router.get("/document-template/get", getDocumentTemplate);
router.post("/document-template/create", createDocumentTemplate);

export default router;
