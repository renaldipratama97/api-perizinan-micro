import express from "express";
import {
  getUserByNIK,
  updateUser,
  Register,
  Login,
} from "../controllers/UserController.js";

const router = express.Router();

router.post("/auth/register", Register);
router.post("/auth/login", Login);
router.get("/user/:nik", getUserByNIK);
router.post("/user/:nik", updateUser);

export default router;
