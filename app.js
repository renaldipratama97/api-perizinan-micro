import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import multer from "multer";
import bodyParser from "body-parser";
import path from "path";
import { fileURLToPath } from "url";
import db from "./src/config/db.js";
// import PermohonanIzin from "./src/models/PermohonanPerizinan.js";
import UserRouter from "./src/routers/UserRouter.js";
import PerizinanRouter from "./src/routers/PerizinanRouter.js";
import DocumentRouter from "./src/routers/DocumentRouter.js";
dotenv.config();
const app = express();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

try {
  db.authenticate();
  console.log("db connected");
  // PermohonanIzin.sync();
} catch (error) {
  console.log(error);
}
app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.json());

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "documents");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().getTime() + "-" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpeg" ||
    file.mimetype === "application/pdf"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single("file")
);

//Static router for image
app.use("/documents", express.static(path.join(__dirname, "documents")));

// All router
app.use(UserRouter);
app.use(PerizinanRouter);
app.use(DocumentRouter);

app.listen(1500, () => console.log("server running in port 1500"));
